package com.mckesson.mscs.cc.productservice.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.mckesson.mscs.cc.productservice.bapi.RfcGetPricesBAPI;
import com.mckesson.mscs.cc.productservice.bapi.RfcGetPricesBAPIInputVO;
import com.mckesson.mscs.cc.productservice.bapi.RfcGetPricesBAPIOutputVO;
import com.mckesson.mscs.cc.productservice.client.AccountResourceClient;
import com.mckesson.mscs.cc.productservice.model.Account;
import com.mckesson.mscs.cc.productservice.model.Product;
import com.mckesson.mscs.cc.productservice.model.SalesArea;
import com.mckesson.mscs.cc.common.MSCSKeys;
import com.mckesson.mscs.cc.common.connection.MSCSConnectionManager;
import com.mckesson.mscs.cc.common.exception.MSCSSystemException;
import com.mckesson.mscs.cc.common.service.LoggingServices;
import com.mckesson.mscs.cc.common.util.GenericComparator;
import com.mckesson.mscs.cc.common.util.MSCSServerUtils;



public class ProductService {
	
	private static final String log = ProductService.class.getSimpleName() + " >> ";

	public List<Product> getAllProducts(String soldToNumber, boolean priceAll, int pricingCnt) throws MSCSSystemException{
		RfcGetPricesBAPIOutputVO outputVO =  getProducts(soldToNumber, "", null, priceAll, pricingCnt);
		return outputVO.getProducts();
	}
	
	public List<Product> getProductsByIDs (String soldToNumber, boolean priceAll, int pricingCnt, List<String> productIDs) throws MSCSSystemException{
		RfcGetPricesBAPIOutputVO outputVO =  getProducts(soldToNumber, null, productIDs, priceAll, pricingCnt);
		return outputVO.getProducts();
	}
	
	public Product getProductById (String soldToNumber, String productId) throws MSCSSystemException{
		List<String> productIDs = new ArrayList<String>();
		productIDs.add(productId);
		RfcGetPricesBAPIOutputVO outputVO =  getProducts(soldToNumber, null, productIDs, true, 1);
		if(outputVO.getProducts() != null && outputVO.getProducts().size() >0)
			return outputVO.getProducts().get(0);
		
		return null;
	}
	

	public List<Product> search(String soldToNumber, String searchQuery, boolean priceAll, int pricingCnt) throws MSCSSystemException{
		RfcGetPricesBAPIOutputVO outputVO =  getProducts(soldToNumber, searchQuery, null, priceAll, pricingCnt);
		return outputVO.getProducts();
	}
	
	public List<Product> getProductsByPricing(List<Product> products, String soldToNumber, String sortBy, String sortOrder) throws MSCSSystemException{
		
		if(products == null || products.size() <=0 )
			return products;
		
		// sort items
		if(sortBy != null && !sortBy.trim().isEmpty() && products != null && products.size()>1)
		this.sortItems(products,sortBy,sortOrder);
		
		List<String> productIDs = new ArrayList<String>();
		// get product id's for first 20 products
		if (products.size() >= 20) {
			List<Product> productSubList = products.subList(0, 20);
			for (Product product : productSubList) {
				productIDs.add(product.getId());
			}
		} else {
			for (Product product : products) {
				productIDs.add(product.getId());
			}
		}
		LoggingServices.info("productIDs size - "+productIDs.size() +"---productIDs--" +productIDs.toString());
		
		//priced items
		List<Product> pricedVO = getProductsByIDs(soldToNumber, true,
				productIDs.size(), productIDs);
	
		if(pricedVO.size() > 0){
			for(Product pricedProduct : pricedVO){
				for(Product p : products){
					if(p.getId().equals(pricedProduct.getId()) && p.getSalesArea() != null && pricedProduct.getSalesArea() != null &&
								p.getSalesArea().getSalesOrganization().equals(pricedProduct.getSalesArea().getSalesOrganization())){
						p.setUnitPrice(pricedProduct.getUnitPrice());
						p.setUnitPriceNetOfProgramDiscount(pricedProduct.getUnitPriceNetOfProgramDiscount());
						p.setListPrice(pricedProduct.getListPrice());
						p.setCustomerPrice(pricedProduct.getCustomerPrice());
						p.setCustomerEligible(pricedProduct.isCustomerEligible());
						p.setAtpQuantityValue(pricedProduct.getAtpQuantityValue());
						p.setEarlyPayDiscount(pricedProduct.getEarlyPayDiscount());
						p.setPricePerBillingUnit(pricedProduct.getPricePerBillingUnit());
						p.setTotalPrice(pricedProduct.getTotalPrice());
						p.setPricedBySAP(true);
					}
				}
			}
		}
		return products;
	}

/*if(sortBy != null && !sortBy.trim().isEmpty() && outputVO.getProducts() != null && outputVO.getProducts().size()>1){
	this.sortItems(outputVO.getProducts(), sortBy, sortOrder);
	if(pricingCnt >1 && !priceAll){
		List<String> needsPricing = new ArrayList<String>();
		for(int i=0; i<outputVO.getProducts().size() && i<pricingCnt; i++ ){
			Product p = outputVO.getProducts().get(i);
			if(p.getUnitPrice() <=0.0)
				needsPricing.add(p.getId());
		}
		
		//Price items needsPricing
		if(needsPricing.size() > 0){
			RfcGetPricesBAPIOutputVO pricedVO = getProducts(soldToNumber, null, needsPricing,  false, needsPricing.size());
			for(Product pricedProduct : pricedVO.getProducts()){
				for(Product p : outputVO.getProducts()){
					if(p.getId().equals(pricedProduct.getId()) && p.getSalesArea() != null && pricedProduct.getSalesArea() != null &&
								p.getSalesArea().getSalesOrganization().equals(pricedProduct.getSalesArea().getSalesOrganization())){
						p.setUnitPrice(pricedProduct.getUnitPrice());
						p.setUnitPriceNetOfProgramDiscount(pricedProduct.getUnitPriceNetOfProgramDiscount());
						p.setListPrice(pricedProduct.getListPrice());
						p.setCustomerPrice(pricedProduct.getCustomerPrice());
						p.setCustomerEligible(pricedProduct.isCustomerEligible());
						p.setAtpQuantityValue(pricedProduct.getAtpQuantityValue());
						p.setEarlyPayDiscount(pricedProduct.getEarlyPayDiscount());
						p.setPricePerBillingUnit(pricedProduct.getPricePerBillingUnit());
						p.setTotalPrice(pricedProduct.getTotalPrice());
					}
				}
			}
		}
	}

}*/
	
	private RfcGetPricesBAPIOutputVO getProducts(String soldToNumber, String searchQuery, List<String> productIDs, 
					boolean priceAll, int pricingCnt) throws MSCSSystemException{
		
		soldToNumber = MSCSServerUtils.padZeroes(soldToNumber, 10);
		
		RfcGetPricesBAPIInputVO inputVO = new RfcGetPricesBAPIInputVO();
		inputVO.setSoldToNumber(soldToNumber);
		inputVO.setPriceAll(priceAll);
		inputVO.setSearchText(searchQuery);
		inputVO.setPriceRecordsCnt(pricingCnt);
		inputVO.setProductIDs(productIDs);
		inputVO.setShipTos(getShipTos(soldToNumber));
				
		RfcGetPricesBAPI bapi = new RfcGetPricesBAPI();
		RfcGetPricesBAPIOutputVO outputVO= bapi.getProducts(inputVO);
		
		return outputVO;
	}
	
	private List<Product> sortItems(List<Product> itemList, String field, String sortOrder){
		
		boolean ascFlag = sortOrder != null && !sortOrder.trim().isEmpty() && sortOrder.toUpperCase().contains("DESC") ? false : true;
		String sortFieldApi = "get" + field.substring(0, 1).toUpperCase() + field.substring(1);
		boolean isNumberSortFieldValue = sortFieldApi.contains("getUnitPrice") || sortFieldApi.contains("getUnitPriceNetOfProgramDiscount") ? true : false; 
		try{
				Collections.sort(itemList, 
						new GenericComparator<Product, Long>(Product.class, sortFieldApi, ascFlag, isNumberSortFieldValue)
						);	
		}catch(Exception e){
			LoggingServices.error("Unable to sort"+ e.getMessage());
		}
		return itemList;
	}
	
	/*
	 * Client code to fetch or lookup the ship to accounts available for specific sold to account
	 */
	private List<RfcGetPricesBAPIInputVO.ShipToAccount> getShipTos(String accountId) throws MSCSSystemException{
		AccountResourceClient client = new AccountResourceClient();
		List<Account> shipTos = client.getShipToAccounts(accountId);
		List<RfcGetPricesBAPIInputVO.ShipToAccount> shipToAccounts = new ArrayList<RfcGetPricesBAPIInputVO.ShipToAccount>();
		for(Account acct : shipTos){
			for(SalesArea s : acct.getSalesAreas()){
				RfcGetPricesBAPIInputVO.ShipToAccount sacct = new RfcGetPricesBAPIInputVO.ShipToAccount();
				sacct.setSalesArea( new SalesArea(s.getSalesOrganization(), s.getDistributionChannel(), s.getDivision()));
				sacct.setShipToNumber(acct.getId());
				shipToAccounts.add(sacct);
			}
		}
		return shipToAccounts;
	}
	
	public  List<String> getGenericEquivalents (String productId)  throws MSCSSystemException{
		
		String logStr = log + "getGenericEquivalents() >> productId = " + productId;
		List <String> genericEquivalents=null;
		Connection connection=null;
		ResultSet resultSet =null;
		Statement stmt =null;		
		StringBuffer sqlStringBuffer =new StringBuffer();

		try {
			String clientId = MSCSConnectionManager.getECCClient();
			String eccDBSchemaName = MSCSConnectionManager.getECCDBSchemaName();

			sqlStringBuffer.append(" SELECT ");
			sqlStringBuffer.append(" A.GENERIC AS GENERIC_EQUIV ");
			sqlStringBuffer.append(" FROM ");
			sqlStringBuffer.append(eccDBSchemaName+".ZMDT_WEB_GENERIC A ");
			sqlStringBuffer.append(" WHERE ");
			sqlStringBuffer.append(" A.MATNR='"+ MSCSServerUtils.padZeroes(productId, 18) +"' AND ");
			sqlStringBuffer.append(" A.MANDT='"+clientId+"'");

			String sql = sqlStringBuffer.toString();
			connection = MSCSConnectionManager.getECCDBConnection();
			if(connection!=null){
				stmt = connection.createStatement();
				resultSet = stmt.executeQuery(sql);
				genericEquivalents = new ArrayList<String>();
				while(resultSet.next()){
					String id = resultSet.getString("GENERIC_EQUIV");
					id = MSCSServerUtils.trimZeroes(id);
					genericEquivalents.add(id);
				}
			}
		}catch(SQLException e){
			LoggingServices.error(logStr + e.getMessage(), e);
			throw new MSCSSystemException(MSCSKeys.SYS_ERR_MSG);					
		}
		finally{
			MSCSConnectionManager.close(stmt, resultSet, connection);
		}
		
		LoggingServices.info(logStr + " Output : " + genericEquivalents);
		return genericEquivalents;
	}	
}
