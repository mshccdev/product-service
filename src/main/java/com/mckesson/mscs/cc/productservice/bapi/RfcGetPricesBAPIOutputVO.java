/**
 * 
 */
package com.mckesson.mscs.cc.productservice.bapi;

import java.util.List;

import com.mckesson.mscs.cc.productservice.model.Product;


/**
 * @author Saravanan Vijayappan
 *
 */
public class RfcGetPricesBAPIOutputVO {

	private List<Product> products;

	/**
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}

	/**
	 * @param products the products to set
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RfcGetPricesBAPIOutputVO [products=");
		builder.append(products);
		builder.append("]");
		return builder.toString();
	}

}
