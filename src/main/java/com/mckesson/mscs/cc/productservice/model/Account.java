/**
 * 
 */
package com.mckesson.mscs.cc.productservice.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Saravanan Vijayappan
 *
 */
@XmlRootElement
@XmlType(propOrder = {"id", "address", "parentId", "groupType",  "salesAreas", "shipTos", "payers"})
public class Account {
	
	private String id;
	private Address address;
	private String parentId;
	private String groupType;
	private List<Account> shipTos;
	private List<Account> payers;
	private List<SalesArea> salesAreas;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}
	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	/**
	 * @return the groupType
	 */
	public String getGroupType() {
		return groupType;
	}
	/**
	 * @param groupType the groupType to set
	 */
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	/**
	 * @return the shipTos
	 */
	public List<Account> getShipTos() {
		return shipTos;
	}
	/**
	 * @param shipTos the shipTos to set
	 */
	public void setShipTos(List<Account> shipTos) {
		this.shipTos = shipTos;
	}
	/**
	 * @return the payers
	 */
	public List<Account> getPayers() {
		return payers;
	}
	/**
	 * @param payers the payers to set
	 */
	public void setPayers(List<Account> payers) {
		this.payers = payers;
	}
	/**
	 * @return the salesAreas
	 */
	public List<SalesArea> getSalesAreas() {
		return salesAreas;
	}
	/**
	 * @param salesAreas the salesAreas to set
	 */
	public void setSalesAreas(List<SalesArea> salesAreas) {
		this.salesAreas = salesAreas;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Account [id=");
		builder.append(id);
		builder.append(", address=");
		builder.append(address);
		builder.append(", groupType=");
		builder.append(groupType);
		builder.append(", parentId=");
		builder.append(parentId);
		builder.append(", shipTos=");
		builder.append(shipTos);
		builder.append(", payers=");
		builder.append(payers);
		builder.append(", salesAreas=");
		builder.append(salesAreas);
		builder.append("]");
		return builder.toString();
	}
	
}

