/**
 * 
 */
package com.mckesson.mscs.cc.productservice.model;

/**
 * @author erzq6oo
 *
 */
public class SalesArea {
	
	private String salesOrganization;
	private String distributionChannel;
	private String division;
	
	public SalesArea() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public SalesArea(String salesOrganization, String distributionChannel,
			String division) {
		super();
		this.salesOrganization = salesOrganization;
		this.distributionChannel = distributionChannel;
		this.division = division;
	}
	/**
	 * @return the salesOrganization
	 */
	public String getSalesOrganization() {
		return salesOrganization;
	}
	/**
	 * @param salesOrganization the salesOrganization to set
	 */
	public void setSalesOrganization(String salesOrganization) {
		this.salesOrganization = salesOrganization;
	}
	/**
	 * @return the distributionChannel
	 */
	public String getDistributionChannel() {
		return distributionChannel;
	}
	/**
	 * @param distributionChannel the distributionChannel to set
	 */
	public void setDistributionChannel(String distributionChannel) {
		this.distributionChannel = distributionChannel;
	}
	/**
	 * @return the division
	 */
	public String getDivision() {
		return division;
	}
	/**
	 * @param division the division to set
	 */
	public void setDivision(String division) {
		this.division = division;
	}
	
	
}
