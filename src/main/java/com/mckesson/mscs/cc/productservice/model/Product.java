/**
 * 
 */
package com.mckesson.mscs.cc.productservice.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author erzq6oo
 *
 */
@XmlRootElement
public class Product {

	private String id;
    private SalesArea salesArea;
    private String description;
    private String ndcNumber;
    private String genericName;
    private String manufacturerPartNumber;
    private String manufacturerName;
    private String catalogNumber;
    private String unit;
   
    private double unitPrice;
    private double listPrice;
    private double customerPrice;
    private double unitPriceNetOfProgramDiscount;
    private double earlyPayDiscount;
    private boolean isOnContract;
   
    private String promoText;

    private double totalPrice;
    private String pricePerBillingUnit;
    private String aspPlus6PerBillingUnit;
    private String aspPlus4Pt3PerBillingUnit;
   
    private double atpQuantityValue;
    private boolean genericAltAvailable;

    private List<Product> genericAlternatives;

    private String billingUnit;
    private String hcpcCode;
    private String billingUnitsPerPackage;
    private String billingUnitsPerNDC;

    private boolean isItemDiscontinued;
    private boolean isCustomerEligible;
    private boolean isLimitedSupply;
    private String salesText;
    private double pacSize;

    private boolean isDropship;
    private boolean isRefrigerated;
    private String itemCategoryGroup;

    private double awpPackagePricing;
    private double awpUnitPricing;
    private String classification;
    private String productType;
    private boolean isGeneric;
    private boolean isPrescription;
    private String strength;
    private boolean isPricedBySAP;
    
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the salesArea
	 */
	public SalesArea getSalesArea() {
		return salesArea;
	}
	/**
	 * @param salesArea the salesArea to set
	 */
	public void setSalesArea(SalesArea salesArea) {
		this.salesArea = salesArea;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the ndcNumber
	 */
	public String getNdcNumber() {
		return ndcNumber;
	}
	/**
	 * @param ndcNumber the ndcNumber to set
	 */
	public void setNdcNumber(String ndcNumber) {
		this.ndcNumber = ndcNumber;
	}
	/**
	 * @return the genericName
	 */
	public String getGenericName() {
		return genericName;
	}
	/**
	 * @param genericName the genericName to set
	 */
	public void setGenericName(String genericName) {
		this.genericName = genericName;
	}
	/**
	 * @return the manufacturerPartNumber
	 */
	public String getManufacturerPartNumber() {
		return manufacturerPartNumber;
	}
	/**
	 * @param manufacturerPartNumber the manufacturerPartNumber to set
	 */
	public void setManufacturerPartNumber(String manufacturerPartNumber) {
		this.manufacturerPartNumber = manufacturerPartNumber;
	}
	
	/**
	 * @return the manufacturerName
	 */
	public String getManufacturerName() {
		return manufacturerName;
	}
	/**
	 * @param manufacturerName the manufacturerName to set
	 */
	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}
	/**
	 * @return the catalogNumber
	 */
	public String getCatalogNumber() {
		return catalogNumber;
	}
	/**
	 * @param catalogNumber the catalogNumber to set
	 */
	public void setCatalogNumber(String catalogNumber) {
		this.catalogNumber = catalogNumber;
	}
	/**
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}
	/**
	 * @param unit the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}
	/**
	 * @return the unitPrice
	 */
	public double getUnitPrice() {
		return unitPrice;
	}
	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	/**
	 * @return the listPrice
	 */
	public double getListPrice() {
		return listPrice;
	}
	/**
	 * @param listPrice the listPrice to set
	 */
	public void setListPrice(double listPrice) {
		this.listPrice = listPrice;
	}
	/**
	 * @return the customerPrice
	 */
	public double getCustomerPrice() {
		return customerPrice;
	}
	/**
	 * @param customerPrice the customerPrice to set
	 */
	public void setCustomerPrice(double customerPrice) {
		this.customerPrice = customerPrice;
	}
	/**
	 * @return the unitPriceNetOfProgramDiscount
	 */
	public double getUnitPriceNetOfProgramDiscount() {
		return unitPriceNetOfProgramDiscount;
	}
	/**
	 * @param unitPriceNetOfProgramDiscount the unitPriceNetOfProgramDiscount to set
	 */
	public void setUnitPriceNetOfProgramDiscount(
			double unitPriceNetOfProgramDiscount) {
		this.unitPriceNetOfProgramDiscount = unitPriceNetOfProgramDiscount;
	}
	
	/**
	 * @return the earlyPayDiscount
	 */
	public double getEarlyPayDiscount() {
		return earlyPayDiscount;
	}
	/**
	 * @param earlyPayDiscount the earlyPayDiscount to set
	 */
	public void setEarlyPayDiscount(double earlyPayDiscount) {
		this.earlyPayDiscount = earlyPayDiscount;
	}
	/**
	 * @return the isOnContract
	 */
	public boolean isOnContract() {
		return isOnContract;
	}
	/**
	 * @param isOnContract the isOnContract to set
	 */
	public void setOnContract(boolean isOnContract) {
		this.isOnContract = isOnContract;
	}
	/**
	 * @return the promoText
	 */
	public String getPromoText() {
		return promoText;
	}
	/**
	 * @param promoText the promoText to set
	 */
	public void setPromoText(String promoText) {
		this.promoText = promoText;
	}
	/**
	 * @return the totalPrice
	 */
	public double getTotalPrice() {
		return totalPrice;
	}
	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	/**
	 * @return the pricePerBillingUnit
	 */
	public String getPricePerBillingUnit() {
		return pricePerBillingUnit;
	}
	/**
	 * @param pricePerBillingUnit the pricePerBillingUnit to set
	 */
	public void setPricePerBillingUnit(String pricePerBillingUnit) {
		this.pricePerBillingUnit = pricePerBillingUnit;
	}
	/**
	 * @return the aspPlus6PerBillingUnit
	 */
	public String getAspPlus6PerBillingUnit() {
		return aspPlus6PerBillingUnit;
	}
	/**
	 * @param aspPlus6PerBillingUnit the aspPlus6PerBillingUnit to set
	 */
	public void setAspPlus6PerBillingUnit(String aspPlus6PerBillingUnit) {
		this.aspPlus6PerBillingUnit = aspPlus6PerBillingUnit;
	}
	/**
	 * @return the aspPlus4Pt3PerBillingUnit
	 */
	public String getAspPlus4Pt3PerBillingUnit() {
		return aspPlus4Pt3PerBillingUnit;
	}
	/**
	 * @param aspPlus4Pt3PerBillingUnit the aspPlus4Pt3PerBillingUnit to set
	 */
	public void setAspPlus4Pt3PerBillingUnit(String aspPlus4Pt3PerBillingUnit) {
		this.aspPlus4Pt3PerBillingUnit = aspPlus4Pt3PerBillingUnit;
	}
	/**
	 * @return the atpQuantityValue
	 */
	public double getAtpQuantityValue() {
		return atpQuantityValue;
	}
	/**
	 * @param atpQuantityValue the atpQuantityValue to set
	 */
	public void setAtpQuantityValue(double atpQuantityValue) {
		this.atpQuantityValue = atpQuantityValue;
	}
	/**
	 * @return the genericAltAvailable
	 */
	public boolean isGenericAltAvailable() {
		return genericAltAvailable;
	}
	/**
	 * @param genericAltAvailable the genericAltAvailable to set
	 */
	public void setGenericAltAvailable(boolean genericAltAvailable) {
		this.genericAltAvailable = genericAltAvailable;
	}

	/**
	 * @return the genericAlternatives
	 */
	public List<Product> getGenericAlternatives() {
		return genericAlternatives;
	}
	/**
	 * @param genericAlternatives the genericAlternatives to set
	 */
	public void setGenericAlternatives(List<Product> genericAlternatives) {
		this.genericAlternatives = genericAlternatives;
	}
	/**
	 * @return the billingUnit
	 */
	public String getBillingUnit() {
		return billingUnit;
	}
	/**
	 * @param billingUnit the billingUnit to set
	 */
	public void setBillingUnit(String billingUnit) {
		this.billingUnit = billingUnit;
	}
	/**
	 * @return the hcpcCode
	 */
	public String getHcpcCode() {
		return hcpcCode;
	}
	/**
	 * @param hcpcCode the hcpcCode to set
	 */
	public void setHcpcCode(String hcpcCode) {
		this.hcpcCode = hcpcCode;
	}
	/**
	 * @return the billingUnitsPerPackage
	 */
	public String getBillingUnitsPerPackage() {
		return billingUnitsPerPackage;
	}
	/**
	 * @param billingUnitsPerPackage the billingUnitsPerPackage to set
	 */
	public void setBillingUnitsPerPackage(String billingUnitsPerPackage) {
		this.billingUnitsPerPackage = billingUnitsPerPackage;
	}
	/**
	 * @return the billingUnitsPerNDC
	 */
	public String getBillingUnitsPerNDC() {
		return billingUnitsPerNDC;
	}
	/**
	 * @param billingUnitsPerNDC the billingUnitsPerNDC to set
	 */
	public void setBillingUnitsPerNDC(String billingUnitsPerNDC) {
		this.billingUnitsPerNDC = billingUnitsPerNDC;
	}
	/**
	 * @return the isItemDiscontinued
	 */
	public boolean isItemDiscontinued() {
		return isItemDiscontinued;
	}
	/**
	 * @param isItemDiscontinued the isItemDiscontinued to set
	 */
	public void setItemDiscontinued(boolean isItemDiscontinued) {
		this.isItemDiscontinued = isItemDiscontinued;
	}
	/**
	 * @return the isCustomerEligible
	 */
	public boolean isCustomerEligible() {
		return isCustomerEligible;
	}
	/**
	 * @param isCustomerEligible the isCustomerEligible to set
	 */
	public void setCustomerEligible(boolean isCustomerEligible) {
		this.isCustomerEligible = isCustomerEligible;
	}
	/**
	 * @return the isLimitedSupply
	 */
	public boolean isLimitedSupply() {
		return isLimitedSupply;
	}
	/**
	 * @param isLimitedSupply the isLimitedSupply to set
	 */
	public void setLimitedSupply(boolean isLimitedSupply) {
		this.isLimitedSupply = isLimitedSupply;
	}
	/**
	 * @return the salesText
	 */
	public String getSalesText() {
		return salesText;
	}
	/**
	 * @param salesText the salesText to set
	 */
	public void setSalesText(String salesText) {
		this.salesText = salesText;
	}
	/**
	 * @return the pacSize
	 */
	public double getPacSize() {
		return pacSize;
	}
	/**
	 * @param pacSize the pacSize to set
	 */
	public void setPacSize(double pacSize) {
		this.pacSize = pacSize;
	}
	/**
	 * @return the isDropship
	 */
	public boolean isDropship() {
		return isDropship;
	}
	/**
	 * @param isDropship the isDropship to set
	 */
	public void setDropship(boolean isDropship) {
		this.isDropship = isDropship;
	}
	/**
	 * @return the isRefrigerated
	 */
	public boolean isRefrigerated() {
		return isRefrigerated;
	}
	/**
	 * @param isRefrigerated the isRefrigerated to set
	 */
	public void setRefrigerated(boolean isRefrigerated) {
		this.isRefrigerated = isRefrigerated;
	}
	/**
	 * @return the itemCategoryGroup
	 */
	public String getItemCategoryGroup() {
		return itemCategoryGroup;
	}
	/**
	 * @param itemCategoryGroup the itemCategoryGroup to set
	 */
	public void setItemCategoryGroup(String itemCategoryGroup) {
		this.itemCategoryGroup = itemCategoryGroup;
	}
	/**
	 * @return the awpPackagePricing
	 */
	public double getAwpPackagePricing() {
		return awpPackagePricing;
	}
	/**
	 * @param awpPackagePricing the awpPackagePricing to set
	 */
	public void setAwpPackagePricing(double awpPackagePricing) {
		this.awpPackagePricing = awpPackagePricing;
	}
	/**
	 * @return the awpUnitPricing
	 */
	public double getAwpUnitPricing() {
		return awpUnitPricing;
	}
	/**
	 * @param awpUnitPricing the awpUnitPricing to set
	 */
	public void setAwpUnitPricing(double awpUnitPricing) {
		this.awpUnitPricing = awpUnitPricing;
	}
	/**
	 * @return the classification
	 */
	public String getClassification() {
		return classification;
	}
	/**
	 * @param classification the classification to set
	 */
	public void setClassification(String classification) {
		this.classification = classification;
	}
	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}
	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}
	/**
	 * @return the isGeneric
	 */
	public boolean isGeneric() {
		return isGeneric;
	}
	/**
	 * @param isGeneric the isGeneric to set
	 */
	public void setGeneric(boolean isGeneric) {
		this.isGeneric = isGeneric;
	}
	/**
	 * @return the isPrescription
	 */
	public boolean isPrescription() {
		return isPrescription;
	}
	/**
	 * @param isPrescription the isPrescription to set
	 */
	public void setPrescription(boolean isPrescription) {
		this.isPrescription = isPrescription;
	}
	/**
	 * @return the strength
	 */
	public String getStrength() {
		return strength;
	}
	/**
	 * @param strength the strength to set
	 */
	public void setStrength(String strength) {
		this.strength = strength;
	}
	
	public boolean isPricedBySAP() {
		return isPricedBySAP;
	}
	public void setPricedBySAP(boolean isPricedBySAP) {
		this.isPricedBySAP = isPricedBySAP;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [aspPlus4Pt3PerBillingUnit=");
		builder.append(aspPlus4Pt3PerBillingUnit);
		builder.append(", aspPlus6PerBillingUnit=");
		builder.append(aspPlus6PerBillingUnit);
		builder.append(", atpQuantityValue=");
		builder.append(atpQuantityValue);
		builder.append(", awpPackagePricing=");
		builder.append(awpPackagePricing);
		builder.append(", awpUnitPricing=");
		builder.append(awpUnitPricing);
		builder.append(", billingUnit=");
		builder.append(billingUnit);
		builder.append(", billingUnitsPerNDC=");
		builder.append(billingUnitsPerNDC);
		builder.append(", billingUnitsPerPackage=");
		builder.append(billingUnitsPerPackage);
		builder.append(", catalogNumber=");
		builder.append(catalogNumber);
		builder.append(", classification=");
		builder.append(classification);
		builder.append(", customerPrice=");
		builder.append(customerPrice);
		builder.append(", description=");
		builder.append(description);
		builder.append(", earlyPayDiscount=");
		builder.append(earlyPayDiscount);
		builder.append(", genericAltAvailable=");
		builder.append(genericAltAvailable);
		builder.append(", genericAlternatives=");
		builder.append(genericAlternatives);
		builder.append(", genericName=");
		builder.append(genericName);
		builder.append(", hcpcCode=");
		builder.append(hcpcCode);
		builder.append(", id=");
		builder.append(id);
		builder.append(", isCustomerEligible=");
		builder.append(isCustomerEligible);
		builder.append(", isDropship=");
		builder.append(isDropship);
		builder.append(", isGeneric=");
		builder.append(isGeneric);
		builder.append(", isItemDiscontinued=");
		builder.append(isItemDiscontinued);
		builder.append(", isLimitedSupply=");
		builder.append(isLimitedSupply);
		builder.append(", isOnContract=");
		builder.append(isOnContract);
		builder.append(", isPrescription=");
		builder.append(isPrescription);
		builder.append(", isRefrigerated=");
		builder.append(isRefrigerated);
		builder.append(", itemCategoryGroup=");
		builder.append(itemCategoryGroup);
		builder.append(", listPrice=");
		builder.append(listPrice);
		builder.append(", manufacturerName=");
		builder.append(manufacturerName);
		builder.append(", manufacturerPartNumber=");
		builder.append(manufacturerPartNumber);
		builder.append(", ndcNumber=");
		builder.append(ndcNumber);
		builder.append(", pacSize=");
		builder.append(pacSize);
		builder.append(", pricePerBillingUnit=");
		builder.append(pricePerBillingUnit);
		builder.append(", productType=");
		builder.append(productType);
		builder.append(", promoText=");
		builder.append(promoText);
		builder.append(", salesArea=");
		builder.append(salesArea);
		builder.append(", salesText=");
		builder.append(salesText);
		builder.append(", strength=");
		builder.append(strength);
		builder.append(", totalPrice=");
		builder.append(totalPrice);
		builder.append(", unit=");
		builder.append(unit);
		builder.append(", unitPrice=");
		builder.append(unitPrice);
		builder.append(", unitPriceNetOfProgramDiscount=");
		builder.append(unitPriceNetOfProgramDiscount);
		builder.append(", isPricedBySAP=");
		builder.append(isPricedBySAP);
		builder.append("]");
		return builder.toString();
	}	
}
