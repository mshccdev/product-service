/**
 * 
 */
package com.mckesson.mscs.cc.productservice.client;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import com.mckesson.mscs.cc.productservice.model.Account;
import com.mckesson.mscs.cc.common.MSCSKeys;
import com.mckesson.mscs.cc.common.exception.MSCSSystemException;



/**
 * @author Saravanan Vijayappan
 *
 */
public class AccountResourceClient {
	
	private static final String log = AccountResourceClient.class.getSimpleName() + " : ";
	private Client client = ClientBuilder.newClient();
    
	public List<Account> getShipToAccounts(String soldToId) throws MSCSSystemException{
		String logStr = log + ">> getShipToAccounts() : accountId = "+ soldToId + " - ";
		WebTarget target =  client.target(MSCSKeys.BASE_ACCOUNT_URL).path(soldToId).path("shipto");
		System.out.println(logStr + "Calling endpoint - "+ target.getUri());
		Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
		List<Account> shipToAccounts = invocationBuilder.get(new GenericType<List<Account>>(){});
		return shipToAccounts;
	}
}
