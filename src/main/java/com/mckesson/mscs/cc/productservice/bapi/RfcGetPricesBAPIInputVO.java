/**
 * 
 */
package com.mckesson.mscs.cc.productservice.bapi;

import java.util.List;

import com.mckesson.mscs.cc.productservice.model.SalesArea;

/**
 * @author Saravanan Vijayappan
 *
 */
public class RfcGetPricesBAPIInputVO {

	public static class ShipToAccount {
		private String shipToNumber;
		private SalesArea salesArea;
		
		public ShipToAccount() {
			super();
			// TODO Auto-generated constructor stub
		}
		
		public ShipToAccount(String shipToNumber, SalesArea salesArea) {
			super();
			this.shipToNumber = shipToNumber;
			this.salesArea = salesArea;
		}
		/**
		 * @return the shipToNumber
		 */
		public String getShipToNumber() {
			return shipToNumber;
		}
		/**
		 * @param shipToNumber the shipToNumber to set
		 */
		public void setShipToNumber(String shipToNumber) {
			this.shipToNumber = shipToNumber;
		}
		/**
		 * @return the salesArea
		 */
		public SalesArea getSalesArea() {
			return salesArea;
		}
		/**
		 * @param salesArea the salesArea to set
		 */
		public void setSalesArea(SalesArea salesArea) {
			this.salesArea = salesArea;
		}
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "ShipToAccount [shipToNumber="
					+ shipToNumber + ", salesArea=" + salesArea + "]";
		}
		
	}
	
	private String soldToNumber;
	private boolean priceAll;
	private int priceRecordsCnt;
	private String searchText;
	private List<String> productIDs;
	private List<ShipToAccount> shipTos;
	
	/**
	 * @return the soldToNumber
	 */
	public String getSoldToNumber() {
		return soldToNumber;
	}
	/**
	 * @param soldToNumber the soldToNumber to set
	 */
	public void setSoldToNumber(String soldToNumber) {
		this.soldToNumber = soldToNumber;
	}
	
	/**
	 * @return the priceAll
	 */
	public boolean isPriceAll() {
		return priceAll;
	}
	/**
	 * @param priceAll the priceAll to set
	 */
	public void setPriceAll(boolean priceAll) {
		this.priceAll = priceAll;
	}
	/**
	 * @return the priceRecordsCnt
	 */
	public int getPriceRecordsCnt() {
		return priceRecordsCnt;
	}
	/**
	 * @param priceRecordsCnt the priceRecordsCnt to set
	 */
	public void setPriceRecordsCnt(int priceRecordsCnt) {
		this.priceRecordsCnt = priceRecordsCnt;
	}
	/**
	 * @return the searchText
	 */
	public String getSearchText() {
		return searchText;
	}
	/**
	 * @param searchText the searchText to set : Search Query 
	 */
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	/**
	 * @return the productIDs
	 */
	public List<String> getProductIDs() {
		return productIDs;
	}
	/**
	 * @param productIDs the productIDs to set
	 */
	public void setProductIDs(List<String> productIDs) {
		this.productIDs = productIDs;
	}
	/**
	 * @return the shipTos
	 */
	public List<ShipToAccount> getShipTos() {
		return shipTos;
	}
	/**
	 * @param shipTos the shipTos to set
	 */
	public void setShipTos(List<ShipToAccount> shipTos) {
		this.shipTos = shipTos;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RfcGetPricesBAPIInputVO [priceAll=");
		builder.append(priceAll);
		builder.append(", priceRecordsCnt=");
		builder.append(priceRecordsCnt);
		builder.append(", productIDs=");
		builder.append(productIDs);
		builder.append(", searchText=");
		builder.append(searchText);
		builder.append(", shipTos=");
		builder.append(shipTos);
		builder.append(", soldToNumber=");
		builder.append(soldToNumber);
		builder.append("]");
		return builder.toString();
	}

	
}
