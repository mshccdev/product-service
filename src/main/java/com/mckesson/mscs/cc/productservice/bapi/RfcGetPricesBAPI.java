/**
 * 
 */
package com.mckesson.mscs.cc.productservice.bapi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.resource.ResourceException;
import javax.resource.cci.ConnectionFactory;
import javax.resource.cci.Interaction;
import javax.resource.cci.MappedRecord;
import javax.resource.cci.RecordFactory;

import com.mckesson.mscs.cc.common.MSCSKeys;
import com.mckesson.mscs.cc.common.connection.EccSupport;
import com.mckesson.mscs.cc.common.connection.MSCSConnectionManager;
import com.mckesson.mscs.cc.common.exception.MSCSSystemException;
import com.mckesson.mscs.cc.common.service.LoggingServices;
import com.mckesson.mscs.cc.common.util.DecimalFormatFunctions;
import com.mckesson.mscs.cc.common.util.MSCSServerUtils;
import com.mckesson.mscs.cc.productservice.bapi.RfcGetPricesBAPIInputVO.ShipToAccount;
import com.mckesson.mscs.cc.productservice.model.Product;
import com.mckesson.mscs.cc.productservice.model.SalesArea;



/**
 * @author Saravanan Vijayappan
 *
 */
public class RfcGetPricesBAPI {

	private static final String log = RfcGetPricesBAPI.class.getSimpleName() + ">> ";

	final static private double ATP_DROPSHIP_QTY = 9999999999d;
	private static final String PRODUCT_HIERARCHY_GENERIC = "00P20";
	private static final String FINE_LINE_GROUP_RX = "1002";
	public static final String BAPI = "Z_RFC_GET_PRICES";
	private static final String UNABLE_TO_PROCESS_ERROR_MSG="Currently System is unable to process your request. Please contact support team.";
	private static final String ABAP_TRUE = "X";
	final static public String FIELD_SEPARATOR = "|";
	
	
	@SuppressWarnings("unchecked")
	public RfcGetPricesBAPIOutputVO getProducts(RfcGetPricesBAPIInputVO inputVO) throws MSCSSystemException{
		
		String logStr = Thread.currentThread().getId() + " - " + log + ">> getProducts >> ";
		
		LoggingServices.info(logStr + " begins " + inputVO.toString());
		
		RfcGetPricesBAPIOutputVO outputVO = new RfcGetPricesBAPIOutputVO();
		List<Product> products = new ArrayList<Product>();
		outputVO.setProducts(products);
		boolean isPricingRqrd;
	
		try {
			EccSupport ecc = new EccSupport(BAPI);
			MappedRecord input = ecc.getInputMap();
			
			//Input SoldTo number
			input.put("SOLD_TO", MSCSServerUtils.padZeroes(inputVO.getSoldToNumber(), 10));
			
			if(inputVO.isPriceAll()){
				isPricingRqrd = true;
				//Input number of records to be priced, 0 - Indicates price all items
				input.put("MAX_PRICES", 0);
				LoggingServices.info(logStr + " Pricing all items");
			}else{
				if(inputVO.getPriceRecordsCnt() <=0 ){
					isPricingRqrd = false;
					LoggingServices.info(logStr + " Pricing is not required");
					//Input number of records to be priced, we need to pass at least 1 to avoid pricing all items
					input.put("MAX_PRICES", 1);
				}else{
					isPricingRqrd = true;
					LoggingServices.info(logStr + " Pricing will be done for only  " + inputVO.getPriceRecordsCnt()  + "  records.");
					//Input number of records to be priced
					input.put("MAX_PRICES", inputVO.getPriceRecordsCnt());
				}
			}
			
			
			//Input ShipTo account details
			if(inputVO.getShipTos() != null){
				ResultSet pricing = (ResultSet) input.get("PRICING");
				for(ShipToAccount shiptos : inputVO.getShipTos()){
					pricing.moveToInsertRow();
					pricing.updateString("SHIP_TO", shiptos.getShipToNumber());
					pricing.updateString("SALES_ORG", shiptos.getSalesArea().getSalesOrganization());
					pricing.updateString("CHANNEL", shiptos.getSalesArea().getDistributionChannel());
					pricing.updateString("DIVISION", shiptos.getSalesArea().getDivision());
				}
			}else{
				LoggingServices.error(logStr + "No ShipTos found - " + inputVO.toString());
			}

			// Input Search Text if any
			if (inputVO.getSearchText() != null && !inputVO.getSearchText().trim().isEmpty()) {
				LoggingServices.info(logStr + "populating search Text =  " + inputVO.getSearchText());
				input.put("SEARCH_STRING", inputVO.getSearchText().toUpperCase());
			}
			
			//Input material IDs
			if(inputVO.getProductIDs() != null && inputVO.getProductIDs().size()>0){
				LoggingServices.info(logStr + "populating material IDs =  " + inputVO.getProductIDs());
				ResultSet productIDResultSet = (ResultSet) input.get("MAT_PRICE");
				for(String productId : inputVO.getProductIDs()){
					productIDResultSet.moveToInsertRow();
					productIDResultSet.updateString("MATERIAL", MSCSServerUtils.padZeroes(productId, 18));
				}
			}

			// Execute BAPI
			MappedRecord ouput = ecc.runInteraction();
			///
			
			// Read BAPI Output data
			ResultSet productDetailTable =  (ResultSet) ouput.get("MAT_PRICE");
			
			StringBuilder sb = new StringBuilder();
			sb.append(logStr + " readProductDetail() logs below \n");
			while (productDetailTable.next()) {
				Product product = readProductDetail(productDetailTable, isPricingRqrd, inputVO.getSoldToNumber(), sb);
				sb.append("\n");
				products.add(product);
			}
			
			updateItemCharacteristics(products);
			
			LoggingServices.debug(sb.toString());
			
			
		}   catch (ResourceException e) {
			LoggingServices.error(e.getMessage(), e);
			throw new MSCSSystemException(UNABLE_TO_PROCESS_ERROR_MSG);
		}  catch (SQLException e) {
			LoggingServices.error(e.getMessage(), e);
			throw new MSCSSystemException("Error occurred while executing BAPI : " + BAPI);
		}
		
		LoggingServices.info(logStr + " Output - " + outputVO.toString());
		return outputVO;
	}
	
	
	/**
	 * @param fineLineGroup
	 * @throws MSCSSystemException
	 * @throws ResourceException
	 * @throws SQLException
	 */
	private static void getCatalogSearchAdvancedFieldValuesRFC(Set<String> inventoryType,
			Set<String> fineLineGroup) throws MSCSSystemException, ResourceException,
			SQLException {

		javax.resource.cci.Connection connection = null;
		Interaction interaction = null;

		try {
			ConnectionFactory connectionFactory = 
				MSCSConnectionManager.getECCJcoConnectionFactoryForSystemUser();
			RecordFactory recordFactory = connectionFactory.getRecordFactory();
			connection = connectionFactory.getConnection();
			interaction = connection.createInteraction();

			MappedRecord rfc = recordFactory.createMappedRecord(
					"Z_RFC_GET_ADVSRCH_FIELD_VALUES");
			rfc.put("GET_INVENTORYTYPE", ABAP_TRUE);
			rfc.put("GET_FINELINESUBGROUP", ABAP_TRUE);

			MappedRecord rfcResults = (MappedRecord) interaction.execute(
					null, rfc);
			
			ResultSet invTypeRs = (ResultSet) rfcResults.get("INVENTORY_TYPE");

			while(invTypeRs.next()) {
				String description = invTypeRs.getString("ATWTB");
				String value = invTypeRs.getString("ATWRT");

				//Results already come in sorted alphabetically
				if ((description != null) && (value != null))
					inventoryType.add(description.trim() + 
							FIELD_SEPARATOR +
							value.trim());

			}

			ResultSet fineLineGroupRs = (ResultSet) rfcResults.get(
			"FINE_LINE_SUB_GROUP");

			while(fineLineGroupRs.next()) {
				String description = fineLineGroupRs.getString("ATWTB");
				String value = fineLineGroupRs.getString("ATWRT");

				//Results already come in sorted alphabetically
				if ((description != null) && (value != null))
					fineLineGroup.add(description.trim() +
							FIELD_SEPARATOR +
							value.trim());

			}
		} finally {
			if (interaction != null) {
				try {
					interaction.close();
				} catch (ResourceException e) {
					e.printStackTrace();
				}
			}

			try {
				MSCSConnectionManager.releaseECCJcoConnection(connection);
			} catch (MSCSSystemException e) {
				e.printStackTrace();
			}


		}
	}
	
	
	private static void updateItemCharacteristics(List<Product> products)
			throws MSCSSystemException, ResourceException, SQLException {

		final int DESCRIPTION = 0;
		final int VALUE = 1;
		HashSet<String> fineLineGroup = new HashSet<String>();
		HashSet<String> inventoryType = new HashSet<String>();

		getCatalogSearchAdvancedFieldValuesRFC(inventoryType,fineLineGroup);

		for (Product product : products) {

			for (Iterator<String> it = inventoryType.iterator(); it.hasNext();) {
				String[] fields = it.next().split("\\" + FIELD_SEPARATOR);
				if ((fields.length >= 2) && (product.getProductType() != null)) {
					if (product.getProductType().compareTo(fields[VALUE]) == 0) {
						product.setProductType(fields[DESCRIPTION]);
						break;
					}
				} else {
					product.setProductType("");
					break;
				}

			}

			for (Iterator<String> it = fineLineGroup.iterator(); it.hasNext();) {
				String[] fields = it.next().split("\\" + FIELD_SEPARATOR);
				if ((fields.length >= 2) && (product.getClassification() != null)) {
					if (product.getClassification().compareTo(fields[VALUE]) == 0) {
						product.setClassification(fields[DESCRIPTION]);
						break;
					}
				} else {
					product.setClassification("");
					break;
				}
			}
		}
	}
	
	/**
	 * Populates the Product Model with the Material details resulted from Z_RFC_GET_PRICES
	 * 
	 * @param product
	 * @param productDetailTable
	 * @param addlInfoPromptSet 
	 * @param isPricingReqd
	 * @throws SQLException as MSCSSystemException
	 */

	private static Product readProductDetail (ResultSet productDetailTable, boolean isPricingReqd, String soldToAcct, StringBuilder logger)
			throws MSCSSystemException {

		double unitPrice=0.0d;
		double pricePerBillingUnit=0.0d;
		SalesArea salesAreaData = new SalesArea();
		Product product = null;
		try{
			
			String itemId=productDetailTable.getString("MATNR");
			product = new Product();
			product.setId(MSCSServerUtils.trimZeroes(itemId));
			logger.append("For Product "+ product.getId() );
			logger.append("\n----------------------------------\n");

			String salesOrg = productDetailTable.getString("VKORG");
			salesAreaData.setSalesOrganization(salesOrg);									
			String distributionChannel = productDetailTable.getString("VTWEG");
			salesAreaData.setDistributionChannel(distributionChannel);			
			String division = productDetailTable.getString("SPART");
			salesAreaData.setDivision(division);			
			product.setSalesArea(salesAreaData);

			String prat1 = productDetailTable.getString("PRAT1");
			String prat2 = productDetailTable.getString("PRAT2");

			double atpQuantity=productDetailTable.getDouble("AVAILABILITY");
			product.setAtpQuantityValue(atpQuantity);

			if (atpQuantity >= ATP_DROPSHIP_QTY)
				product.setDropship(true);
			else
				product.setDropship(false);

			String refrigerated = productDetailTable.getString("REFRIGERATED");
			product.setRefrigerated((refrigerated.equalsIgnoreCase("X")) ? true : false);

			if(isPricingReqd){
				String unitPriceStr = productDetailTable.getString("PRICE");
				unitPrice= DecimalFormatFunctions.parseStringToDouble(unitPriceStr);
				product.setUnitPrice(unitPrice);

			}
			else{
				unitPrice = product.getUnitPrice();
			}	

			/* List Price, Regular Price and Promo Description added for Promotional discount visibility*/

			String listPriceStr = productDetailTable.getString("LIST_PRICE");
			String regPriceStr = productDetailTable.getString("REG_CUST_PRICE");

			//List Price
			double listPrice = DecimalFormatFunctions.parseStringToDouble(listPriceStr);
			//Customer Regular Price
			double regPrice = DecimalFormatFunctions.parseStringToDouble(regPriceStr);

			product.setEarlyPayDiscount(MSCSServerUtils.priceStringToDecimal(
					productDetailTable.getString("EARLY_PAY_ST_DISCOUNT")));

			/* Override 
			 * 1. the list price with the regular price if the list price is lesser than the reg price.
			 * 2. list price with Unit Price if Unit Price is greater than list Price.
			 * 3. regular Price with Unit Price if Unit Price is greater than regular Price.
			 */

			if(listPrice<regPrice)
				listPrice=regPrice;
			if(unitPrice > listPrice)
				listPrice = unitPrice;
			if(unitPrice > regPrice)
				regPrice = unitPrice;

			//Promo Description
			String promoText = productDetailTable.getString("PROMO_TEXT");

			//Limited supply flag
			String limitedSupply = productDetailTable.getString("LIMITED_SUPPLY");
			//Package size

			String pacSizeStr=productDetailTable.getString("SIZ");
			double pacSize=DecimalFormatFunctions.parseStringToDouble(pacSizeStr);


			if(limitedSupply.equalsIgnoreCase("Y"))
				product.setLimitedSupply(true);
			else
				product.setLimitedSupply(false);

			// Removed unit price 0$ check for punchout
			//if(((prat1!=null && prat1.equalsIgnoreCase("X")) || (prat2!=null && prat2.equalsIgnoreCase("X"))) || !(unitPrice >0.0d)){
				if(((prat1!=null && prat1.equalsIgnoreCase("X")) || (prat2!=null && prat2.equalsIgnoreCase("X")))){

				product.setCustomerEligible(false);
				logger.append("\n");
				logger.append(product.getId()+" has either PRAT1 set to true or PRAT2 set to true or Unit Price is zero.Please check material data in material master.");

			}else{
				product.setCustomerEligible(true);

			}
			product.setUnitPrice(unitPrice);
			product.setListPrice(listPrice);
			product.setCustomerPrice(regPrice);
			product.setPromoText(promoText);
			product.setPacSize(pacSize);

			product.setDescription(productDetailTable.getString("MAKTX"));
			product.setGenericName(productDetailTable.getString("GENERIC_NAME"));
			String ndcNumber=productDetailTable.getString("EAN11");
			if(ndcNumber!=null&& ndcNumber.trim().length()>0)
				product.setNdcNumber(MSCSServerUtils.formatNDC(ndcNumber.trim()));
			product.setManufacturerName(productDetailTable.getString("MANUFACTURER_NAM"));
			product.setManufacturerPartNumber(productDetailTable.getString("MFRPN"));
			product.setCatalogNumber(productDetailTable.getString("BISMT"));
			product.setUnit(productDetailTable.getString("MEINS"));
			product.setBillingUnit(productDetailTable.getString("BILLING_UNIT_1"));
			product.setHcpcCode(productDetailTable.getString("HCPC_1"));
			product.setBillingUnitsPerPackage(productDetailTable.getString("BILL_UNIT_1_PKG"));
			product.setBillingUnitsPerNDC(productDetailTable.getString("BILL_UNIT_1_NDC"));
			String aspPlus6PerBillingUnit = productDetailTable.getString("BILLING_PRICE_1");
			product.setItemCategoryGroup(productDetailTable.getString("MTPOS"));
			
			if(aspPlus6PerBillingUnit!=null && !aspPlus6PerBillingUnit.trim().equalsIgnoreCase(""))
				product.setAspPlus6PerBillingUnit(aspPlus6PerBillingUnit);
			else
				product.setAspPlus6PerBillingUnit("N/A");

			// Added for new column aspPlus4.3PerBillingUnit
			if (aspPlus6PerBillingUnit != null && !aspPlus6PerBillingUnit.trim().equalsIgnoreCase("")) {
				try{
					double aspPlus6PerBillingUt = Double.parseDouble(aspPlus6PerBillingUnit);
					double aspPlus4Pt3PerBillingUnit = aspPlus6PerBillingUt * MSCSKeys.ASP_VALUE;
					DecimalFormat decimalFormat = new DecimalFormat("#.##");
					decimalFormat.setRoundingMode(RoundingMode.DOWN);
					product.setAspPlus4Pt3PerBillingUnit(decimalFormat.format(aspPlus4Pt3PerBillingUnit));
				}catch(NumberFormatException e){
					logger.append("\n");
					logger.append( " Exception Occured aspPlus4.3PerBillingUnit for ProductId : " + product.getId() + e.getMessage());
				}
			} else {
				product.setAspPlus4Pt3PerBillingUnit("N/A");
			}

			// Started block -- by Amit Jain(eg0r8dt) for temporary allowing CSOS functionality for some account.
			//(remove this whole block once enable CSOS for all eligible customers) 
			if(product.getItemCategoryGroup() != null && product.getItemCategoryGroup().equalsIgnoreCase(MSCSKeys.ZDC2)){
				if(checkCSOSEligibility(soldToAcct, product.getId()))
					product.setCustomerEligible(true);
				else
					product.setCustomerEligible(false);
			}
			//(remove this whole block once enable CSOS for all eligible customers)
			// Ended block -- by Amit Jain(eg0r8dt) for temporary allowing CSOS functionality for some account.
			
			/* Price Per Billing Unit : BOC
			 * Logic for Price Per Billing Unit : 
			 * If Unit is not PAK divide UnitPrice by BillingUnitsPerNDC
			 * If Unit is PAK and Product Description contains either TAB or CAP then divide UnitPrice by BillingUnitsPerPackage else divide UnitPrice by BillingUnitsPerNDC
			 * Unit Price Calculation Logic : 
			 * If Unit is PAK and Product Description contains either TAB or CAP then unit price is not modified else modify the unit price and divide unit price by itemSize.
			 */

			if(product.getUnit()!=null && 
					product.getUnitPrice()>0.0d &&
					product.getDescription()!=null && 
					(product.getBillingUnitsPerPackage()!=null && DecimalFormatFunctions.parseStringToDouble(product.getBillingUnitsPerPackage())>0.0d) && 
					(product.getBillingUnitsPerNDC()!=null && DecimalFormatFunctions.parseStringToDouble(product.getBillingUnitsPerNDC())>0.0d)){

				double itemSize = product.getPacSize(); 
				double itemUnitPrice = product.getUnitPrice();

				if(itemSize<=0.0d){
					itemSize=1.0d;
				}

				//commenting the part as requested by user on 07/19/2015 --Amit Jain
				
				//Adding the below part to display product is in contract or not.
				String contractPrice = productDetailTable.getString("COCO_NUM");
				logger.append("\n");
				logger.append("Product: " + product.getId() + " is in contract :" + ((contractPrice == null || "".equals(contractPrice)) ? false  : true) + " and contractPrice value is: "+ contractPrice);
				product.setOnContract(contractPrice == null || "".equals(contractPrice) ? false  : true);
				
/*				if(product.getUnit().equalsIgnoreCase("PAK")){

					if(product.getDescription().contains("TAB") || product.getDescription().contains("CAP")){

						pricePerBillingUnit = itemUnitPrice/DecimalFormatFunctions.parseStringToDouble(product.getBillingUnitsPerPackage());

					}else{
						double modifiedItemUnitPrice = itemUnitPrice/itemSize;
						pricePerBillingUnit = modifiedItemUnitPrice/DecimalFormatFunctions.parseStringToDouble(product.getBillingUnitsPerNDC());
					}
				}else{
					double modifiedItemUnitPrice = itemUnitPrice/itemSize;
					pricePerBillingUnit = modifiedItemUnitPrice/DecimalFormatFunctions.parseStringToDouble(product.getBillingUnitsPerNDC());
				}	*/
				
				//Added below line for billing unit price calculation change requested by user on 07/19/2015 --Amit Jain
			
			   pricePerBillingUnit = itemUnitPrice/DecimalFormatFunctions.parseStringToDouble(product.getBillingUnitsPerPackage());

				product.setPricePerBillingUnit(String.valueOf(pricePerBillingUnit));		
			}else{
				logger.append("\n");
				logger.append(" Either Unit Price or Unit or Description or BillingUnitsPerPackage or BillingUnitsPerNDC is null or <=0. >>> Problem occured in setting the Price Per Billing Unit For Product...: "+product.getId());
				product.setPricePerBillingUnit("N/A");
			}
			/*Price Per Billing Unit : EOC*/
			String salesText = productDetailTable.getString("SALES_TEXT");

			if(product.isLimitedSupply()){
				salesText=salesText.concat(" \n Due to limited supply, no backorders accepted for this product.");
			}
			if(salesText!=null && salesText.trim().length()>0){
				product.setSalesText(salesText);
			}

			if(productDetailTable.getString("GEN_EQUIV_EXISTS")!=null && productDetailTable.getString("GEN_EQUIV_EXISTS").equalsIgnoreCase("X")){
				product.setGenericAltAvailable(true);
			}
			else{
				product.setGenericAltAvailable(false);
			}

			BigDecimal awpPkg = productDetailTable.getBigDecimal("AWP_PKG");
			if (awpPkg != null)
				product.setAwpPackagePricing(awpPkg.doubleValue());

			BigDecimal awpUnit = productDetailTable.getBigDecimal("AWP_UNIT");
			if (awpUnit != null)
				product.setAwpUnitPricing(awpUnit.doubleValue());

			String itemClassification = productDetailTable.getString(
					"FINE_LINE_SUB_GR");
			product.setClassification((itemClassification == null) ? 
					"" : itemClassification);

			String productType = productDetailTable.getString("INVENTORY_TYPE"); 
			product.setProductType((productType == null) ? "" : productType);

			String strength = productDetailTable.getString("STRENGTH");
			product.setStrength((strength == null) ? "" : strength);

			String productHierarchy = productDetailTable.getString("PRDHA");
			if ((productHierarchy != null) && 
					(productHierarchy.startsWith(PRODUCT_HIERARCHY_GENERIC))) {
				product.setGeneric(true);
			}

			String fineLineGroup = productDetailTable.getString(
					"FINE_LINE_GROUP");
			if ((fineLineGroup != null) && (fineLineGroup.compareToIgnoreCase(
					FINE_LINE_GROUP_RX) == 0)) {
				product.setPrescription(true);
			}

			calculateItemPriceNetOfProgramDiscount(product);
			logger.append("\n----------------------------------\n\n");
		}
		catch (SQLException e) {
			LoggingServices.error(log + e.getMessage(), e);
			throw new MSCSSystemException(MSCSKeys.SYS_ERR_MSG);
		}
		
		return product;
	}
	
	/**
	 * Calculate the price net of program discount of an items using the
	 * discounts populated in that item. <code>item</code>'s 
	 * unitPriceNetOfProgramDiscount will be updated.
	 * @param item the item that has the price in need of calculation
	 */
	private static void calculateItemPriceNetOfProgramDiscount(Product product) {
		BigDecimal unitPrice = BigDecimal.valueOf(product.getUnitPrice());

		BigDecimal netPrice = unitPrice
		.add(BigDecimal.valueOf(product.getEarlyPayDiscount()));

		product.setUnitPriceNetOfProgramDiscount(netPrice.doubleValue());
	}
	
	/**This is method is used to search in database for sold to account 
	 * and material combination for CSOS eligibility.
	 * @param soldToAcct
	 * @param material
	 * @return true/false
	 * @throws MSCSSystemException
	 */
		private static boolean checkCSOSEligibility(String soldToAcct, String material) throws MSCSSystemException {
			int count = 0;
			String sql = null;
			Connection connection =  null;
			PreparedStatement pstmnt = null;
			ResultSet rs = null;
			
			try {
					connection = MSCSConnectionManager.getWebstoreDBConnection();
					String dbSchema =  MSCSConnectionManager.getWebstoreDBSchemaName();
					sql = "SELECT 1 FROM " + dbSchema + ".Z_CSOS_CUST_MAT_ELIGIBILITY WHERE SOLDTO = ? AND MATERIAL_ID = ? " +
							"AND START_DATE <= SYSDATE AND END_DATE >= SYSDATE AND DELETED = 0";
					pstmnt = connection.prepareStatement(sql);
					pstmnt.setString(1, soldToAcct);
					pstmnt.setString(2, material);
					rs = pstmnt.executeQuery();
					if (rs.next())
				      count = rs.getInt(1);
					LoggingServices.debug("Sold to Account:"	+ soldToAcct + ":  Material:" +  material +": Count:" + count);
					if(count > 0) {
						return true;
					}
				}	catch (SQLException e) {
				LoggingServices.error(e.getMessage(), e);
				throw new MSCSSystemException("Currently System is unable to process your request. Please contact support team.");
			}
			finally
			{
				LoggingServices.debug("Releasing ReportsDB connection");
				MSCSConnectionManager.releaseWebstoreDBConnection(connection);
				if(pstmnt != null)
				{
					try {
						pstmnt.close();
					} catch (SQLException e) {
						LoggingServices.error(e.getMessage(), e);
					}
				}
				if(rs !=null)
				{
					try {
						rs.close();
					} catch (SQLException e) {
						LoggingServices.error(e.getMessage(), e);
					}
				}
			}
				return false;
		}
}
