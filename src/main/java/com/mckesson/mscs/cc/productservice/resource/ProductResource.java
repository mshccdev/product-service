/**
 * 
 */
package com.mckesson.mscs.cc.productservice.resource;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.mckesson.mscs.cc.productservice.model.Product;
import com.mckesson.mscs.cc.productservice.service.ProductService;
import com.mckesson.mscs.cc.common.exception.MSCSSystemException;
import com.mckesson.mscs.cc.common.service.LoggingServices;
import com.mckesson.mscs.cc.common.util.MSCSServerUtils;

/**
 * @author erzq6oo
 * 
 */
@Path("products")
public class ProductResource {

	private static final String log = ProductResource.class.getSimpleName()+ " >> ";

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProducts(
			@QueryParam("soldto") String soldto,
			@QueryParam("priceall") String priceall,
			@QueryParam("pricingcnt") int pricingCnt,
			@QueryParam("ids") String ids, 
			@Context UriInfo uriInfo)
			throws MSCSSystemException {

		String logStr = log + " >> " + "getProducts() >> ";
		LoggingServices.info(logStr + "Query Params : "
				+ uriInfo.getQueryParameters());
		List<Product> products = null;

		ProductService service = new ProductService();

		// When Product IDs are passed, get only product details of those IDs
		if (ids != null && ids.trim().length() > 0) {
			List<String> productIDs = MSCSServerUtils.getDelimitedStringAsList(
					ids, ",");
			products = service.getProductsByIDs(soldto, toBoolean(priceall),
					pricingCnt, productIDs);
		} else { // Otherwise get all products
			products = service.getAllProducts(soldto, toBoolean(priceall),
					pricingCnt);
		}

		LoggingServices.info(logStr + " Output : " + products);
		return products;
	}

	@GET
    @Path("search")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Product> search(
                  @QueryParam("soldto") String soldto,
                  @QueryParam("query") String query,
                  @QueryParam("priceall") String priceall,
                  @QueryParam("pricingcnt") int pricingCnt,
                  @QueryParam("sortby") String sortBy,
                  @QueryParam("sortorder") String sortOrder,
                  @Context UriInfo uriInfo)
                  throws MSCSSystemException {

           String logStr = log + " >> " + "search() >> ";
           LoggingServices.info(logStr + "Query Params : "
                        + uriInfo.getQueryParameters());

           // Create service
           ProductService service = new ProductService();
           List<Product> needsPricing = null;

           // This will search by setting pricingCnt = 1
           needsPricing = service.search(soldto, decode(query), toBoolean(priceall), pricingCnt);
           
           List<Product> products = service.getProductsByPricing(needsPricing,soldto,sortBy,sortOrder);

           LoggingServices.info(logStr + " Output : " + products);
           return products;
    }



	@GET
	@Path("{productId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Product getProduct(
			@PathParam("productId") String productId,
			@QueryParam("soldto") String soldto,
			@Context UriInfo uriInfo)
			throws MSCSSystemException {

		String logStr = log + " >> " + "getProduct() >> productId: "
				+ productId + " >> ";
		LoggingServices.info(logStr + "Query Params : "
				+ uriInfo.getQueryParameters());

		// Create service
		ProductService service = new ProductService();
		Product product = service.getProductById(soldto, productId);

		return product;
	}
	
	@GET
	@Path("{productId}/generic")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getGenericEquivalents(
			@PathParam("productId") String productId,
			@QueryParam("soldto") String soldto)
			throws MSCSSystemException {

		String logStr = log + " >> " + "getGenericAlternatives() >> productId : "+ productId + " >> ";
		LoggingServices.info(logStr + "SoldTo : "+ soldto);
		
		// Create service
		ProductService service = new ProductService();
		List<String> productIDs = service.getGenericEquivalents(productId);
		return productIDs;
	}

	private boolean toBoolean(String str) {
		return (str != null && str.equalsIgnoreCase("Y")) ? true : false;
	}

	private String decode(String url){
		if(url == null || url.trim().isEmpty())
			return "";
		
		try {
			url = URLDecoder.decode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LoggingServices.error(e.getMessage() + " Query:"+url, e);
		}
		return url;
	}
}